<xsl:stylesheet version="1.0" extension-element-prefixes="dp" exclude-result-prefixes="dp" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="http://www.example.org/tns" xmlns:dp="http://www.datapower.com/extensions">
<xsl:output method="xml"/>
<xsl:template match="/">
<xsl:variable name="output" select="dp:parse(//XmlInputString)"/>
<xsl:copy-of select="$output"/>
</xsl:template>
</xsl:stylesheet>